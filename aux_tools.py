import time,datetime
import pandas as pd
import unicodedata
import codecs

def str_to_date_long(strDate,date_format):
	long_date = time.mktime(datetime.datetime.strptime(strDate, date_format).timetuple())*1000 #"%d/%m/%Y"
	return int(long_date)


def function_dummies(df):
  df_dummies=df
  colnames=df.columns
  for c in colnames:
      if df[c].dtypes=='object':
          col=  pd.get_dummies(df[c])
          df_dummies[c+"_"+col.columns]=col
          df_dummies = df_dummies.drop(labels=[c],axis=1)
  return (df_dummies)


def remove_accents(original):
  in_bytes = bytes(original,"utf-8")
  unicode_string = in_bytes.decode("utf-8")
  nkfd_form = unicodedata.normalize('NFKD', unicode_string)
  return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])


def get_unique(somelist):
  used = set()
  return [x for x in mylist if x not in used and (used.add(x) or True)]

#date_lat = data_aux.apply(lambda dataframe: str(dataframe['lat_lon']).split(",")[0],axis=1)
#date_lon = data_aux.apply(lambda dataframe: str(dataframe['lat_lon']).split(",")[1],axis=1)
